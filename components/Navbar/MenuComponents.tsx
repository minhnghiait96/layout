import * as React from 'react'
import {Icon,Dropdown} from 'antd'
interface Props{
    menu:any;
}
const MenuComponents:React.FunctionComponent<Props> = (props:Props) => {
    const arrDropMenu = [{
        IconType:'gift',
        IconDrop:'down',
        Title:'Mega Menu'
    },
    {
        IconType:'project',
        IconDrop:'down',
        Title:'Project'
    },
    {
        IconType:'setting',
        IconDrop:'down',
        Title:'Setting'
    }
]
    const DropMenu = () => {
        let arrMenu = arrDropMenu.map((e)=>{
            return (
                <Dropdown className="dropDown1" overlay={props.menu} trigger={['click']}>
                    <span >
              <Icon type={e.IconType} theme="twoTone" />
              &nbsp;{e.Title} <Icon type={e.IconDrop} />
              </span>
                </Dropdown>
            )
        }
        
        )
        return arrMenu;
    }
    return (
       <>{DropMenu()}</>
    )
}
export default MenuComponents;
import * as React from 'react'
import { useState } from 'react'
import { Layout, Icon, Menu } from 'antd';

import DashBoard from './DashBoard'
import Notifications from './Notifications';
import ProfileUser from './ProfileUser';
import MenuComponents from './MenuComponents';

interface Props {
  isToggle?: boolean,
  setIsToggle(): void
}

const Navbar: React.FunctionComponent<Props> = (props: Props) => {
  const { Header } = Layout;
const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="#">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="#">2nd menu item</a>
    </Menu.Item>

    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);
  const [isShowSearch, setIsShowSearch] = useState(true);
  const showIconSearch: any = () => {
    setIsShowSearch(!isShowSearch)
    console.log(isShowSearch)
  }

  return (
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%', display: 'flex', backgroundColor: 'white', flexDirection: 'row' }}>
      <div className="leftHeader">
        <img src="./../static/img/2.png" className={props.isToggle?'logo hidden':'logo'}/>
        <Icon type={props.isToggle? 'close': 'menu'} className={props.isToggle? 'iconClose':'iconMenu'}
          onClick={props.setIsToggle}
        />
       
      </div>
      <div className="rightHeader">
        <div className="menuSearch">{isShowSearch ?
          <div className="menuContent">
            <Icon type="search" style={{ marginLeft: '20px' }} onClick={showIconSearch} />
            <MenuComponents menu={menu}></MenuComponents>
          </div>
          :
          <div className="inputHolder">
            <input type="text" placeholder="Type to Search " />
            <div className="iconSearch"><Icon type="search" /></div>
            <div className="iconClose" onClick={() => setIsShowSearch(true)}><Icon type="close" /></div>
          </div>
        }
        </div>
        <div className="profileUser">
          <DashBoard></DashBoard>
          <Notifications></Notifications>
          <ProfileUser></ProfileUser>
          <Icon className="iconMenu" type="menu" />
        </div>
      </div>


    </Header>
  )
}
export default Navbar;
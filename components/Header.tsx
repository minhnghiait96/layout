import * as React from 'react'
import Link from 'next/link'
const Header:React.FunctionComponent = () =>(
<div>
<nav>
        <Link href="/">
          <a>Home</a>
        </Link>{' '}
        |{' '}
        <Link href="/about">
          <a>About</a>
        </Link>{' '}
        |{' '}
        <Link href="/initial-props">
          <a>With Initial Props</a>
        </Link>
      </nav>
</div>
)
export default Header;

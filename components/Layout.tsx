import * as React from 'react'
import {useState} from 'react'
import Sidebar from './Sidebar/Sidebar'
import Navbar from './Navbar/Navbar'
import Footer from './Footer/Footer'
import './../style.css'
import { FakeMenu } from '../mock/menuList'
const Layout: React.FunctionComponent = () => {
  const [isToggle,setIsToggle]=useState(false);
const setToggle = () =>{
  setIsToggle(!isToggle)
}
  return (
  <div className="layout" >

    <header>
    <Navbar isToggle={isToggle} setIsToggle={setToggle}></Navbar>
    </header>
    <div className="main">

      <nav className={isToggle ? 'main-sider-show':'main-sider'}><Sidebar isToggle={isToggle} listSidebar={FakeMenu}/></nav>
      <div className="mainContent">
      <article style={{textAlign:'center'}}>
     
        </article>
      
      <footer><Footer></Footer></footer>
      </div>
    
    </div>
   

  </div>
  )
}

export default Layout

import * as React from 'react'
import { Menu, Icon } from 'antd';
const ListSideBar: React.FunctionComponent = () => {
  const { SubMenu } = Menu;
  const mapArr = () => {
    let arr = MenuSidebar.map((value) => {
      return (
       
        <SubMenu
          key={value.SubMenu.key}
          title={
            <span>
              <Icon type={value.SubMenu.iconType} />
              <span>{value.SubMenu.titleSub}</span>
            </span>
          }
        >
          {value.SubMenu.childrenSub.map((value) => {
            return (
              <SubMenu
                key={value.keySub}
                title={
                  <span>
                    <Icon type={value.iconSub} />
                    <span>{value.titleChildrenSub}</span>
                  </span>
                }
              >
                {value.arrMenuItem.map((value) => {
                  return (
                    <Menu.Item key={value.key}>{value.titleMenu}</Menu.Item>
                  )
                })}
              </SubMenu>
            )
          })}
        </SubMenu>
      )
    })
    return arr;
  }


  const MenuSidebar = [{
    SubMenu: {
      key: 'sub1',
      iconType: 'mail',
      titleSub: 'Menu',
      childrenSub:
        [
          {
            keySub: 'submenu1',
            iconSub: 'mail',
            titleChildrenSub: 'Dashboards',
            arrMenuItem:
              [
                { key: '1', titleMenu: 'Analytics' }, { key: '2', titleMenu: 'Commerce' }]
          },
          {
            keySub: 'submenu2',
            iconSub: 'mail',
            titleChildrenSub: 'Pages',
            arrMenuItem:
              [
                { key: '1', titleMenu: 'Login' }, { key: '2', titleMenu: 'Login Box' }]
          },
        ]
    }
  },
  {
    SubMenu: {
      key: 'sub2',
      iconType: 'mail',
      titleSub: 'Main Content',
      childrenSub:
        [
          {
            keySub: 'submenu1',
            iconSub: 'mail',
            titleChildrenSub: 'Element',
            arrMenuItem:
              [
                { key: '1', titleMenu: 'Start' }, { key: '2', titleMenu: 'Mil' }]
          },
          {
            keySub: 'submenu2',
            iconSub: 'mail',
            titleChildrenSub: 'Component',
            arrMenuItem:
              [
                { key: '1', titleMenu: 'Tabs' }, { key: '2', titleMenu: 'Accord' }]
          },
        ]
    }
  }
  ]
  return (
    <>{mapArr()}</>
  )
}


export default ListSideBar;
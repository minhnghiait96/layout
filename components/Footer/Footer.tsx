import * as React from 'react'
import { Menu } from 'antd';
import NotificationsFooter from './NotificationsFooter'
import FooterMenu from './FooterMenu';
import GridMenuFooter from './GirdMenuFooter';
const Footer:React.FunctionComponent = () =>{
    const menu = (
        <Menu>
          <Menu.Item key="0">
            <a href="http://www.alipay.com/">1st menu item</a>
          </Menu.Item>
          <Menu.Item key="1">
            <a href="http://www.taobao.com/">2nd menu item</a>
          </Menu.Item>
      
          <Menu.Item key="3">3rd menu item</Menu.Item>
        </Menu>
      );
    return (
    <div className="app-footer">
        <div className="left-footer"><NotificationsFooter></NotificationsFooter></div>
        <div className="right-footer">
            <div><FooterMenu menu={menu}></FooterMenu></div>
            <div><GridMenuFooter menu={menu}></GridMenuFooter></div>
        </div>
    </div>
    )
}
export default Footer;
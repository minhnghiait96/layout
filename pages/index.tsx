import * as React from 'react'
import Link from 'next/link'
import Layout from '../components/Layout'
import { NextPage } from 'next'
import '../style.css'

const IndexPage: NextPage = () => {
  return (
    <Layout>
      <input type="button" />
      <div className="example"></div>
      <h1>Hello Next.js 👋</h1>
      <p>
        <Link href="/about">
          <a>About</a>
        
        </Link>
      </p>
    </Layout>
  )
}

export default IndexPage

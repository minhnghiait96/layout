
export type User = {
  id: number
  name: string
}
export type MenuList= {
  Id: number;
  ParentId: number;
  Name: string;
  IconType:string;
  DisplayOrder: number;
  Filhos: MenuList[] | null;
}
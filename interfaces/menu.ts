export interface MenuModel {
    id: string;
    name: string;
    iconType?: string;
    children?: Array<MenuModel>;
  }
  